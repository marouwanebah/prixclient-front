/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EnTeteComponent } from './en-tete.component';

describe('EnTeteComponent', () => {
  let component: EnTeteComponent;
  let fixture: ComponentFixture<EnTeteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnTeteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnTeteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
