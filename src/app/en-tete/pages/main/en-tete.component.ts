import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-en-tete',
  templateUrl: './en-tete.component.html',
  styleUrls: ['./en-tete.component.scss']
})

/**
 * Class en-tete du programme.
 */
export class EnTeteComponent implements OnInit {

  /**
   * Nb article initale dans le panier.
   */
  public nbItemPanier = 0;

  public nomPersonne = 'S\'identifier';

  /**
   * Statut de la connection de l'utilisateur. 
   */
  public connected = false;

  /**
   * Listes des sous catégories.
   */
  public listItem = ['Recharge Telephone', 'Transfert d\'argent'];
  public secondLigne = false;

  public test = 'sqd';
  
  @ViewChild('inputValue') inputValue: ElementRef;
  /**
   * Le constrcuteur.
   */
  constructor() { }

  /**
   * Init du comosant.
   */
  ngOnInit(): void {
  }

  /**
   * Methode retour a l'acceuil. 
   */
  public goToAccueil(): void {
    console.log('acceuil cclicked');
  }

  /**
   * Méthode pour aller au panier.
   */
  public search(): void {
    console.log('acceuil cclicked');
    this.nbItemPanier++;
    this.test = this.inputValue.nativeElement.value;
  }
  /**
   * Méthode pour aller au panier.
   */
  public goToPanier(): void {
    console.log('acceuil cclicked');
    this.nbItemPanier++;
    this.test = this.inputValue.nativeElement.value;
  }

  /**
   * Méthode pour la connection de l'utilisateur.
   */
  public login(): void {
    console.log('login');
    this.test = this.inputValue.nativeElement.value;
  }

  public toggleMenu() {
    console.log('login');
    this.test = this.inputValue.nativeElement.value;
  }

}

