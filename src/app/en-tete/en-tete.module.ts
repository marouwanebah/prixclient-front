import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatBadgeModule } from '@angular/material/badge';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EnTeteComponent } from './pages/main/en-tete.component';

@NgModule({
  declarations: [EnTeteComponent],
  imports: [
    CommonModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatIconModule,
    MatBadgeModule,
    MatExpansionModule,
    FlexLayoutModule
  ],
  exports: [EnTeteComponent, FlexLayoutModule]

})
export class EnTeteModule { }
