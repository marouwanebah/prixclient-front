import { FlexLayoutModule } from '@angular/flex-layout';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AccueilModule } from './accueil/accueil.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EnTeteModule } from './en-tete/en-tete.module';
import { PiedDePageModule } from './pied-de-page/pied-de-page.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FlexLayoutModule,
    AccueilModule,
    EnTeteModule,
    PiedDePageModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
