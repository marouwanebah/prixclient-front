import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PiedDePageComponent } from './pages/main/pied-de-page.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PiedDePageComponent]
})
export class PiedDePageModule { }
