import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccueilComponent } from './pages/main/accueil.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AccueilComponent]
})
export class AccueilModule { }
